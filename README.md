# coach_MTECT-MTE 2023

Projet de réponse à l'appel d'offre du marché coaching pour MTECT-MTE

• Consulter le [dossier de consultation](https://gitlab.com/la_zone/coach_mtect-mte-2023/-/tree/main/dossier-de-consultation)

• Retrouver les notes sur [le pad de suivi](https://hackmd.io/Rz0L-mmPT7q2u0MEI7NdVQ)

• Rejoindre la [salle de visio](https://meet.jit.si/mtect-mte)

